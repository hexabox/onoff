#! /usr/bin/python

import RPi.GPIO as GPIO 
import time
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-s", "--sec", help="Duration in seconds of pressing the button", type=int, default=1)
pressing_time = parser.parse_args().sec

GPIO.setmode(GPIO.BOARD) 
GPIO.setup(7, GPIO.OUT) 
GPIO.output(7,True)
time.sleep(pressing_time)
GPIO.output(7,False)
